import React from "react";
import Home from './Home';

import { createStore } from 'redux';
import { Provider } from 'react-redux';

import appReducer from '../reducers/appReducer'

import "./app.css";

const almacen = createStore(appReducer);

export default () => (
  <Provider store={almacen}>
    <Home />
  </Provider>
);
