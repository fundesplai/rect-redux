import React, { useState } from "react";
import { connect } from "react-redux";

const Nuevo = (props) => {
  const [palabra, setPalabra] = useState("");

  const enviar = () => {
    props.dispatch({
      type: "NOU_ELEMENT",
      item: palabra,
    });
    setPalabra("");
  };

  return (
    <>
      <input
        value={palabra}
        onChange={(e) => setPalabra(e.target.value)}
      />
      <button onClick={enviar}>Enviar</button>
    </>
  );
};

export default connect()(Nuevo);
