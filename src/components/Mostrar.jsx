import React from 'react';
import {connect} from 'react-redux';

const Mostrar = (props) => {
        if (props.lista.length===0) {
            return <h3>No data...</h3>;
        }
        let lis = props.lista.map((el, index) => <li key={index}>{el}</li>);
        return ( 
            <>
                <h4>{props.titulo}</h4>
                <h5>{props.subtitulo}</h5>
                <ul>
                    {lis}
                </ul>
            </>
         );
}
 
const mapStateToProps = (state) => {
    return {
       lista: state.elements,
       titulo: 'lista de cosas'
    }
  }
  
// export default Mostrar;
export default connect(mapStateToProps)(Mostrar);
  
  
