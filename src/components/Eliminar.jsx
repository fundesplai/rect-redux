import React from 'react';
import {connect} from 'react-redux';

const Eliminar = (props) => {
    
   const eliminar = () => {
        props.dispatch({
            type:'NETEJA_ELEMENTS',
           });
    }

    return <button onClick={eliminar} >Borrar</button>;

}

export default connect()(Eliminar);
  
  