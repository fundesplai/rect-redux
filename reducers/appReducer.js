
const appReducer = (state, action) => {
    
    // let newState = state;
    // let newState = {...state};
    let newState = state ? JSON.parse(JSON.stringify(state)) : { elements:[] };

    switch (action.type) {
        // action = {type: 'NOU_ELEMENT', item: 'abcd'}
        case 'NOU_ELEMENT':
            newState.elements.push(action.item);
            return newState;

        // action = {type: 'NETEJA_ELEMENTS'}
        case 'NETEJA_ELEMENTS':
            newState.elements = [];
            return newState;

        default:
            return newState;
    }

}
export default appReducer;